const express = require('express')
const app = require('express')();
const https = require('https');
const fs = require('fs');

app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/instructions', (req, res) => {
    res.render('instructions');
});

app.get('/healthcheck', (req, res) => {
    res.render('healthcheck');
});

app.get('/health', function (req, res){
    console.log('Health check returned 200.')
    res.sendStatus(200)
})

app.get('/adambrauns', (req, res) => {
    res.render('adambrauns');
});

var options = {
    key: fs.readFileSync('./certs/key.pem'),
    cert: fs.readFileSync('./certs/certificate.pem')
}

https.createServer(options, app).listen(443, function () {
    console.log('Hello world app listening on port 443! Browse to https://localhost:443')
});