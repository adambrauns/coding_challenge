FROM node:7
MAINTAINER Adam Brauns (BraunsAJ14@uww.edu)
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD node index.js
EXPOSE 443