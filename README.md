# Requirements
1) Create a project on a public hosted source code management site (Github, Gitlab)

2) Create a small application which hosts a simple, hello-world, webpage. Ideally, a simple Java or NodeJS application will suffice

3) Create a CI pipeline which is triggered when a change is committed to the repo’s master branch which results in a Docker image being built and hosted (example: Dockerhub) via Circle/Travis/Gitlab CI. 

4) Once pipeline is established, modify the application to handle a health-route which returns a HTTP 200. 

5) Update README.md with instructions of how to run the dockerized application locally (assume the end-user has docker running locally). 

6) Bonus features (some ideas to explore if you should have time) - an automated test executor to fail if tests fail, project file linter (run on merge requests), TLS sockets, and/or any terraform or ansible related configuration scripts to compliment the project. 

# Instructions (Running from DockerHub)
1) Pull the latest image from docker hub   

      docker pull adambrauns/coding_challenge:latest
      
2) Run the image in a container opening port 443   

      docker run -it --rm -p 443:443 adambrauns/coding_challenge:latest
      
3) Open your favorite web browser and go to the following URL:   

      https://localhost:443
      
4) Browse the web application

# Instructions (Running from DockerFile)
1) Clone the following repo: https://gitlab.com/adambrauns/coding_challenge   

      git clone https://gitlab.com/adambrauns/coding_challenge.git   
      
2) Build the image via the Dockerfile   

      docker build -t coding_challenge . 
      
3) Run the image in a container opening port 443   

      docker run -it --rm -p 443:443 coding_challenge:latest
      
4) Open your favorite web browser and go to the following URL: 

      https://localhost:443
      
5) Browse the web application

# DockerHub
This project produces a new docker image each time the master branch is modified. Link to the docker image can be found [here](https://cloud.docker.com/repository/docker/adambrauns/coding_challenge).

# Pipelines
This project runs two stages in the pipeline
1) Lintify

   This stage will run pug-lint on all the pug files in the views folder   
2) Package

   This stage will build the docker image and push it to the Docker Hub